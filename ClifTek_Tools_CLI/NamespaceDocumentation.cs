//----------------------------------------------------
//                ClifTek Tools
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
namespace ClifTek_Tools_CLI
{
    /// <summary>
    /// The namespace holding all of the functionality for the Command Line Interface to ClifTek_Tools library.
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    class NamespaceDoc { }
}

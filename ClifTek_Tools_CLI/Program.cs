//----------------------------------------------------
//                ClifTek Tools
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using System;
using CommandLine;

namespace ClifTek_Tools_CLI
{
    /// <summary>
    /// The main program to run.
    /// </summary>
    class Program
    {
        private static string s_invokedVerb;
        private static object s_invokedVerbInstance;

        static void Main(string[] args)
        {
            //var arguments = new System.Collections.Generic.List<string>(args);
            //arguments.Add("filename-operations");
            //arguments.Add("-u");
            //arguments.Add("-f");
            //arguments.Add(@"C:\Users\Thomas\Desktop\VoiceOver");
            //arguments.Add("-r");
            //arguments.Add("-v");
            //args = arguments.ToArray();
            var options = new Options();
            try
            {
                if (!Parser.Default.ParseArguments(args, options, (verb, subOptions) =>
                                                                  {
                                                                      s_invokedVerb = verb;
                                                                      s_invokedVerbInstance = subOptions;
                                                                  }))
                {
                    Environment.Exit(Parser.DefaultExitCodeFail);
                }

                var commonOptions = (CommonOptions)s_invokedVerbInstance;
                commonOptions.ActOnOptions();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}

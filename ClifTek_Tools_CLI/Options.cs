//----------------------------------------------------
//                ClifTek Tools
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using CommandLine;
using CommandLine.Text;

namespace ClifTek_Tools_CLI
{
    /// <summary>
    /// Specifies the available commands.
    /// </summary>
    class Options
    {
        [VerbOption("extension", HelpText = "Perform an operation on file extensions")]
        public ExtensionOptions ExtensionVerb { get; set; }

        [VerbOption("filename-operations", HelpText = "Perform file name operations.")]
        public FileNameOptions FileNameOptionsVerb { get; set; }

        [VerbOption("zip", HelpText = "Perform an operation for a zip archive.")]
        public ZipOptions ZipVerb { get; set; }

        [VerbOption("code-copyrighter", HelpText = "Add a Copyright header to some code.")]
        public CodeCopyrighterOptions CopyrighterVerb { get; set; }
        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpVerbOption]
        public string GetUsage(string zVerb)
        {
            return HelpText.AutoBuild(this, zVerb);
        }
    }
}

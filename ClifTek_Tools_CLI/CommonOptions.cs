//----------------------------------------------------
//                ClifTek Tools
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using CommandLine;

namespace ClifTek_Tools_CLI
{
    /// <summary>
    /// Provides common options used across commands.
    /// </summary>
    public abstract class CommonOptions
    {
        /// <summary>
        /// Gets or sets a value indicating whether or not to run verbose.
        /// </summary>
        /// <value>
        ///   <c>true</c> if verbose; otherwise, <c>false</c>.
        /// </value>
        [Option('v', VerbOptionNames.k_Verbose, HelpText = "Print details during execution.")]
        public bool Verbose { get; set; }

        public abstract void ActOnOptions();
    }
}

//----------------------------------------------------
//                ClifTek Tools
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using CommandLine;
using ClifTek_Tools;

namespace ClifTek_Tools_CLI
{
    /// <summary>
    /// Provides options for the Extension command.
    /// </summary>
    /// <seealso cref="ClifTek_Tools_CLI.CommonOptions" />
    public class ExtensionOptions : CommonOptions
    {
        // - Required Options -

        /// <summary>
        /// Gets or sets the directory to run the Extension command in.
        /// </summary>
        /// <value>
        /// The directory.
        /// </value>
        [Option('f', "folder", Required = true, HelpText = "Directory Path to perform operation in.")]
        public string Directory { get; set; }

        /// <summary>
        /// Gets or sets the extension.
        /// </summary>
        /// <value>
        /// The extension.
        /// </value>
        [Option('e', "extension", Required = true, HelpText = "The extension to perform operations on.")]
        public string Extension { get; set; }

        // - Delete Mutually Exclusive Set -

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ExtensionOptions"/> should run the delete operation.
        /// </summary>
        /// <value>
        ///   <c>true</c> if delete; otherwise, <c>false</c>.
        /// </value>
        [Option('d', "delete", MutuallyExclusiveSet = "del", HelpText = "Delete all files with given extension.")]
        public bool Delete { get; set; }

        // - Swap Mutually Exclusive Set -

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ExtensionOptions"/> should run the swap operation.
        /// </summary>
        /// <value>
        /// The swap extension.
        /// </value>
        [Option('s', "swap", MutuallyExclusiveSet = "swap", HelpText = "Swap given extension with new extension.")]
        public string SwapExtension { get; set; }

        // - Optional Options -

        /// <summary>
        /// Gets or sets a value indicating whether [check sub directories].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [check sub directories]; otherwise, <c>false</c>.
        /// </value>
        [Option('r', "recursive", HelpText = "Perform recursive operation in subdirectories of given directory.")]
        public bool CheckSubDirectories { get; set; }

        public override void ActOnOptions()
        {
            if (Delete == true)
                ExtensionOperations.DoFileDelete(Directory, Extension, CheckSubDirectories, Verbose);
            else if (!string.IsNullOrEmpty(SwapExtension))
                ExtensionOperations.DoExtensionSwap(Directory, Extension, SwapExtension, CheckSubDirectories, Verbose);
        }
    }
}

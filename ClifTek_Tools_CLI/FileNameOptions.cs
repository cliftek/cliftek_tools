﻿using ClifTek_Tools;
using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClifTek_Tools_CLI
{
    public class FileNameOptions : CommonOptions
    {
        // - Uppercase Mutually Exclusive Set -

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="FileNameOptions"/> should run the uppercase operation.
        /// </summary>
        /// <value>
        ///   <c>true</c> if delete; otherwise, <c>false</c>.
        /// </value>
        [Option('u', "upper-case", MutuallyExclusiveSet = "up", HelpText = "Convert Filename(s) to uppercase.")]
        public bool uppercase { get; set; }

        // - Optional Options -

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="FileNameOptions"/> should run the uppercase operation on a specific file.
        /// </summary>
        /// <value>
        ///   <c>true</c> if delete; otherwise, <c>false</c>.
        /// </value>
        [Option('s', "single-file", HelpText = "Specify a single file to operate on.")]
        public string SingleFile { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="FileNameOptions"/> should run the uppercase operation on a specific file.
        /// </summary>
        /// <value>
        ///   <c>true</c> if delete; otherwise, <c>false</c>.
        /// </value>
        [Option('f', "folder", HelpText = "Specify a directory path to perform operation in.")]
        public string Directory { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [check sub directories].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [check sub directories]; otherwise, <c>false</c>.
        /// </value>
        [Option('r', "recursive", HelpText = "Perform recursive operation in subdirectories of given directory.")]
        public bool CheckSubDirectories { get; set; }

        public override void ActOnOptions()
        {
            if (uppercase)
            {
                if (string.IsNullOrEmpty(SingleFile) == false && string.IsNullOrEmpty(Directory))
                {
                    FileNameOperations.MakeUppercase(SingleFile, Verbose);
                }
                else if (string.IsNullOrEmpty(Directory) == false && string.IsNullOrEmpty(SingleFile))
                {
                    FileNameOperations.MakeFilesInDirectoryUppercase(Directory, CheckSubDirectories, Verbose);
                }
                else
                {
                    Console.WriteLine("Please specify s for single file or f for folder.");
                }
            }
        }
    }
}

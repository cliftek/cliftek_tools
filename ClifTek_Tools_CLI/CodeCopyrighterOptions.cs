//----------------------------------------------------
//                ClifTek Tools
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using CommandLine;
using ClifTek_Tools;

namespace ClifTek_Tools_CLI
{
    /// <summary>
    /// Provides options for the Code Copyright command.
    /// </summary>
    /// <seealso cref="ClifTek_Tools_CLI.CommonOptions" />
    public class CodeCopyrighterOptions : CommonOptions
    {
        // - Required Options -

        /// <summary>
        /// Gets or sets the directory to run the Code Copyright command in.
        /// </summary>
        /// <value>
        /// The directory.
        /// </value>
        [Option('f', "folder", Required = true, HelpText = "Directory Path to perform operation in.")]
        public string Directory { get; set; }

        /// <summary>
        /// Gets or sets the path to the file holding the Copyright text..
        /// </summary>
        /// <value>
        /// The copyright path.
        /// </value>
        [Option('c', "copyright-path", Required = true, HelpText = "Path to file containing Copyright text.")]
        public string CopyrightPath { get; set; }

        // - Optional Options -

        /// <summary>
        /// Gets or sets a value indicating whether [check sub directories].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [check sub directories]; otherwise, <c>false</c>.
        /// </value>
        [Option('r', "recursive", HelpText = "Perform recursive operation in subdirectories of given directory.")]
        public bool CheckSubDirectories { get; set; }

        public override void ActOnOptions()
        {
            CodeCopyrighter.CopyrightCode(Directory, CopyrightPath, CheckSubDirectories, Verbose);
        }
    }
}

//----------------------------------------------------
//                ClifTek Tools
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using System;
using CommandLine;
using ClifTek_Tools;

namespace ClifTek_Tools_CLI
{
    /// <summary>
    /// Provides options for the Zip command.
    /// </summary>
    /// <seealso cref="ClifTek_Tools_CLI.CommonOptions" />
    public class ZipOptions : CommonOptions
    {
        // - Required Options -

        /// <summary>
        /// Gets or sets the source path.
        /// </summary>
        /// <value>
        /// The source path.
        /// </value>
        [Option('s', "source", Required = true, HelpText = "The source path for the zip opetration.")]
        public string SrcPath { get; set; }

        /// <summary>
        /// Gets or sets the destination path.
        /// </summary>
        /// <value>
        /// The DST path.
        /// </value>
        [Option('d', "destination", Required = true, HelpText = "The destination path for the zip operation.")]
        public string DstPath { get; set; }

        // - Delete Mutually Exclusive Set -

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ZipOptions"/> should run the compress operation.
        /// </summary>
        /// <value>
        ///   <c>true</c> if compress; otherwise, <c>false</c>.
        /// </value>
        [Option('c', "compress", MutuallyExclusiveSet = "compress", HelpText = "Compress source directory to zip archive at destination directory.")]
        public bool Compress { get; set; }

        // - Swap Mutually Exclusive Set -

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ZipOptions"/> should run the extract operation.
        /// </summary>
        /// <value>
        ///   <c>true</c> if extract; otherwise, <c>false</c>.
        /// </value>
        [Option('e', "extract", MutuallyExclusiveSet = "extract", HelpText = "Expand zip archive from source directory to destination directory.")]
        public bool Extract { get; set; }

        public override void ActOnOptions()
        {
            if (Compress)
            {
                bool result = Zip.Compress(SrcPath, DstPath, Verbose);

                if (result)
                    Console.WriteLine("Zipped archive successfully.");
                else
                    Console.WriteLine("Failed to zip archive.");
            }
            else if (Extract)
            {
                bool result = Zip.Extract(SrcPath, DstPath, Verbose);

                if (result)
                    Console.WriteLine("Unzipped archive successfully.");
                else
                    Console.WriteLine("Failed to unzip archive.");
            }
        }
    }
}

//----------------------------------------------------
//                ClifTek Tools
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
namespace ClifTek_Tools_CLI
{
    /// <summary>
    /// Provides a number of verb option names.
    /// </summary>
    public static class VerbOptionNames
    {
        // - Constants -

        /// <summary>
        /// The Verbose option name
        /// </summary>
        public const string k_Verbose = "verbose";
    }
}

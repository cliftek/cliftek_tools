# ClifTek Tools

**ClifTek Tools provides a number of miscellaneous but useful tools that have come in useful for development at [ClifTek][ClifTek].**

[ClifTek]: http://www.cliftek.com

## Prerequisites

- Windows
- .NET 4.5

## Online Resources

- [Source Code][source]
- [Documentation][documentation]

[source]: https://bitbucket.org/cliftek/cliftek_tools/src
[documentation]: http://www.cliftek.com/documentation/cliftek_tools

## Troubleshooting and support

- Bugs can be submitted via the [issue tracker][issues]

[issues]: https://bitbucket.org/cliftek/cliftek_tools/issues

## Quick Contributing Guide

- Fork and clone locally.
- Create a task specific branch and add your features.
- Test!
- Submit a Pull Request.

## Authors

TJ Clifton

## License

The MIT license (refer to the included LICENSE.md file)
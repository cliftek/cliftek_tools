﻿//----------------------------------------------------
//                ClifTek Tools
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClifTek_Tools
{
    public static class FileNameOperations
    {
        public static void MakeFilesInDirectoryUppercase(string zDirectoryPath, bool zIsRecursive, bool zVerbose)
        {
            //Get all files in directory
            var dirInfo = FileOperations.GetDirectoryInfoFromPath(zDirectoryPath);

            //Find all files that match the extension.
            var files = FileOperations.GetFiles(dirInfo, true, zIsRecursive);

            foreach (var file in files)
            {
                MakeUppercase(file.FullName, zVerbose);
            }
        }

        public static void MakeUppercase(string zFilePath, bool zVerbose = false)
        {
            if (File.Exists(zFilePath) == false)
            {
                Console.WriteLine("No file exists at : " + zFilePath);
                return;
            }

            var fileNameWithExt = Path.GetFileName(zFilePath);
            var ext = Path.GetExtension(zFilePath);
            var currentName = fileNameWithExt.Remove(fileNameWithExt.Length - ext.Length, ext.Length);
            var startPath = zFilePath.Remove(zFilePath.Length - fileNameWithExt.Length, fileNameWithExt.Length);

            var newName = new StringBuilder(currentName);

            newName[0] = char.ToUpper(newName[0]);

            var newPath = startPath + newName + ext;

            if (zVerbose)
            {
                Console.WriteLine(string.Format("Change path from {0} to {1}", zFilePath, newPath));
            }

            File.Move(zFilePath, newPath);
        }
    }
}
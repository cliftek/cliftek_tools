//----------------------------------------------------
//                ClifTek Tools
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using System;
using System.IO;
using System.Collections.Generic;

namespace ClifTek_Tools
{
    /// <summary>
    /// This class provides a number of general purpose file operations.
    /// </summary>
    public static class FileOperations
    {
        /// <summary>
        /// Gets the files in a directory that match a given search pattern.
        /// </summary>
        /// <param name="dirInfo">The directory information.</param>
        /// <param name="zRoot">if set to <c>true</c> [get the files in the root directory as well].</param>
        /// <param name="zIsRecursive">if set to <c>true</c> [get files recursively].</param>
        /// <param name="zSearchPattern">The search pattern to use to find files in the directory.</param>
        /// <returns></returns>
        public static List<FileInfo> GetFiles(DirectoryInfo dirInfo, bool zRoot, bool zIsRecursive, string zSearchPattern = "*")
        {
            List<FileInfo> files = new List<FileInfo>();

            //If recursive then check all subdirectories for files as well
            if (zIsRecursive)
            {
                foreach (var subDir in dirInfo.GetDirectories())
                {
                    files.AddRange(GetFiles(subDir, false, true, zSearchPattern));
                    files.AddRange(subDir.GetFiles(zSearchPattern));
                }
            }

            if (zRoot)
                files.AddRange(dirInfo.GetFiles(zSearchPattern));

            return files;
        }

        /// <summary>
        /// Gets the directory information from a given path.
        /// </summary>
        /// <param name="zPath">The directory path.</param>
        /// <param name="zVerbose">if set to <c>true</c> [run verbose].</param>
        /// <returns></returns>
        public static DirectoryInfo GetDirectoryInfoFromPath(string zPath, bool zVerbose = false)
        {
            DirectoryInfo foundDirectory = null;

            foundDirectory = new DirectoryInfo(zPath);

            if (!foundDirectory.Exists)
                Console.WriteLine("Could not find directory: " + foundDirectory.FullName);
            else if (zVerbose)
                Console.WriteLine("Found Directory: " + foundDirectory.FullName);
            
            return foundDirectory;
        }
    }
}

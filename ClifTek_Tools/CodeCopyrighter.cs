//----------------------------------------------------
//                ClifTek Tools
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using System;
using System.IO;
using System.Collections.Generic;

namespace ClifTek_Tools
{
    /// <summary>
    /// This class provides functionality to deal with copyright headers for code files.
    /// </summary>
    public static class CodeCopyrighter
    {
        // - Constants -
        private const string k_commentHeaderFooter = "//----------------------------------------------------";

        /// <summary>
        /// Adds a specified Copyright header to all C# files in a directory.
        /// </summary>
        /// <param name="zDirectoryPath">The directory path.</param>
        /// <param name="zCopyrightTextFilePath">The copyright text file path.</param>
        /// <param name="zIsRecursive">if set to <c>true</c> [set copyright headers recursively].</param>
        /// <param name="zVerbose">if set to <c>true</c> [run verbose].</param>
        public static void CopyrightCode(string zDirectoryPath, string zCopyrightTextFilePath, bool zIsRecursive, bool zVerbose = false)
        {
            //Make sure the directory exists and bail out with an error if not.
            if (!Directory.Exists(zDirectoryPath))
            {
                Console.WriteLine("Error: Directory '" + zDirectoryPath + "' does not exist");
                return;
            }

            //Get all C# files in the directory.
            SearchOption option = zIsRecursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            var directoryFiles = Directory.GetFiles(zDirectoryPath, "*.cs", option);

            //Get copyright text and set for all found files in directory.
            string copyrightText = getCopyrightText(zCopyrightTextFilePath, zVerbose);

            foreach (var file in directoryFiles)
                setCopyrightHeader(file, copyrightText, zVerbose);
        }

        private static string getCopyrightText(string zFilePath, bool zVerbose)
        {
            //Read all text in the given file.
            try
            {
                using (FileStream fs = File.Open(zFilePath, FileMode.Open))
                {
                    using (StreamReader reader = new StreamReader(fs))
                        return reader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: Failed with exception " + e.Message);
            }

            return "";
        }

        private static void setCopyrightHeader(string zFilePath, string zCopyrightText, bool zVerbose)
        {
            try
            {
                //Open the code file.
                var newFileContents = new List<string>();
                using (FileStream fs = File.Open(zFilePath, FileMode.Open))
                {
                    //Read all contents in code file.
                    using (StreamReader reader = new StreamReader(fs))
                    {
                        //Read all lines to end of stream
                        List<string> fileContents = new List<string>();
                        while (!reader.EndOfStream)
                            fileContents.Add(reader.ReadLine());

                        //Make sure we actually found some contents.
                        if (fileContents.Count > 0)
                        {
                            int linesToRemoveUpTo = -1;
                            //Check to see if there was an old header on the file
                            if (fileContents[0] == k_commentHeaderFooter)
                            {
                                for (int i = 1; i < fileContents.Count; ++i)
                                {
                                    if (fileContents[i] == k_commentHeaderFooter)
                                    {
                                        linesToRemoveUpTo = i;
                                        break;
                                    }
                                }
                            }

                            //If we found an old header then remove
                            if (linesToRemoveUpTo != -1)
                                fileContents.RemoveRange(0, linesToRemoveUpTo + 1);

                            //Add the comment header
                            newFileContents.Add(k_commentHeaderFooter);
                            newFileContents.Add(zCopyrightText);
                            newFileContents.Add(k_commentHeaderFooter);
                            newFileContents.AddRange(fileContents);
                        }
                    }

                    //Write out new file contents.
                    using (FileStream fs2 = File.Create(zFilePath))
                    {
                        //Write out new contents
                        using (StreamWriter writer = new StreamWriter(fs2))
                        {
                            for (int i = 0; i < newFileContents.Count; ++i)
                                writer.WriteLine(newFileContents[i]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: Failed with exception " + e.Message);
            }
        }
    }
}

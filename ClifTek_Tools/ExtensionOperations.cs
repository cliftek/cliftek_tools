//----------------------------------------------------
//                ClifTek Tools
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using System;
using System.IO;
using System.Collections.Generic;

namespace ClifTek_Tools
{
    /// <summary>
    /// This class provides a number of operations that act on files with particular extensions.
    /// </summary>
    public static class ExtensionOperations
    {
        /// <summary>
        /// Changes all files extensions in a directory to a new extension.
        /// </summary>
        /// <param name="zDirectoryPath">The directory path.</param>
        /// <param name="zExtension">The old extension to change.</param>
        /// <param name="zNewExtension">The new extension to set.</param>
        /// <param name="zIsRecursive">if set to <c>true</c> [set extensions recursively].</param>
        /// <param name="zVerbose">if set to <c>true</c> [run verbose].</param>
        public static void DoExtensionSwap(string zDirectoryPath, string zExtension, string zNewExtension, bool zIsRecursive, bool zVerbose = false)
        {
            //Make sure the directory exists and bail out with an error if not.
            if (!Directory.Exists(zDirectoryPath))
            {
                Console.WriteLine("Error: Directory '" + zDirectoryPath + "' does not exist");
                return;
            }

            var files = findFilesWithExtension(zDirectoryPath, zIsRecursive, zExtension, zVerbose);

            //Print out found file names if running verbose.
            if (zVerbose)
            {
                foreach (var file in files)
                    Console.WriteLine("Found file: " + file.FullName);
            }

            //Swap all file extensions
            changeExt(files, zNewExtension);
        }

        /// <summary>
        /// Deletes all files with the given extension in a directory
        /// </summary>
        /// <param name="zDirectoryPath">The directory path.</param>
        /// <param name="zExtension">The extension to delete.</param>
        /// <param name="zIsRecursive">if set to <c>true</c> [delete files recursively].</param>
        /// <param name="zVerbose">if set to <c>true</c> [run verbose].</param>
        public static void DoFileDelete(string zDirectoryPath, string zExtension, bool zIsRecursive, bool zVerbose = false)
        {
            //Make sure the directory exists and bail out with an error if not.
            if (!Directory.Exists(zDirectoryPath))
            {
                Console.WriteLine("Error: Directory '" + zDirectoryPath + "' does not exist");
                return;
            }

            var files = findFilesWithExtension(zDirectoryPath, zIsRecursive, zExtension, zVerbose);

            //Print out found file names if running verbose.
            if (zVerbose)
            {
                foreach (var file in files)
                    Console.WriteLine("Found file: " + file.FullName);
            }

            //Delete all files with given extension.
            deleteFiles(files);
        }

        private static List<FileInfo> findFilesWithExtension(string zDirectoryPath, bool zIsRecursive, string zExtension, bool zVerbose)
        {
            //Get all files in directory
            var dirInfo = FileOperations.GetDirectoryInfoFromPath(zDirectoryPath);

            //Setup the search pattern for the extension we want to delete.
            string ext = zExtension;
            ext = "*" + ext + "*";
            List<FileInfo> files = new List<FileInfo>();

            //Find all files that match the extension.
            return FileOperations.GetFiles(dirInfo, true, zIsRecursive, ext);
        }

        private static void changeExt(List<FileInfo> zFiles, string zNewExt)
        {
            //Swap all files extensions by utilising File.Move
            foreach (var fileInfo in zFiles)
            {
                var filename = Path.ChangeExtension(fileInfo.FullName, zNewExt);
                File.Move(fileInfo.FullName, filename);
            }
        }

        private static void deleteFiles(List<FileInfo> zFiles)
        {
            foreach (var fileInfo in zFiles)
                File.Delete(fileInfo.FullName);
        }
    }
}

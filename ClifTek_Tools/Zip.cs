//----------------------------------------------------
//                ClifTek Tools
//       Copyright © 2015-2016 ClifTek Ltd
//----------------------------------------------------
using System;
using System.IO;
using System.IO.Compression;

namespace ClifTek_Tools
{
    /// <summary>
    /// Provides functionality for dealing with Zip archives.
    /// </summary>
    public static class Zip
    {
        /// <summary>
        /// Compresses a directory to a zip file.
        /// </summary>
        /// <param name="zSrcPath">The source directory path.</param>
        /// <param name="zDstPath">The destination zip file path.</param>
        /// <param name="zIsVerbose">if set to <c>true</c> [run verbose].</param>
        /// <returns>Whether or not the zip file was compressed successfully.</returns>
        public static bool Compress(string zSrcPath, string zDstPath, bool zIsVerbose = false)
        {
            //Make sure the directory exists and bail out with an error if not.
            if (!Directory.Exists(zSrcPath))
            {
                Console.WriteLine("Could not find directory: " + zSrcPath);
                return false;
            }

            //Make sure the user has specified the correct extension.
            if (Path.GetExtension(zDstPath) != ".zip")
            {
                Console.WriteLine("Zip archive path must use extension '.zip'");
                return false;
            }

            ZipFile.CreateFromDirectory(zSrcPath, zDstPath);

            return true;
        }

        /// <summary>
        /// Extracts a directory from a zip file.
        /// </summary>
        /// <param name="zSrcPath">The source directory path.</param>
        /// <param name="zDstPath">The destination directory path.</param>
        /// <param name="zIsVerbose">if set to <c>true</c> [run verbose].</param>
        /// <returns>Whether or not the zip file was extracted successfully.</returns>
        public static bool Extract(string zSrcPath, string zDstPath, bool zIsVerbose = false)
        {
            if (!File.Exists(zSrcPath))
            {
                if (zIsVerbose)
                    Console.WriteLine("Could not find zip archive at path: " + zSrcPath);

                return false;
            }

            if (Path.GetExtension(zSrcPath) != ".zip")
            {
                if (zIsVerbose)
                    Console.WriteLine("Zip archive path must use extension '.zip'");

                return false;
            }

            ZipFile.ExtractToDirectory(zSrcPath, zDstPath);

            return true;
        }
    }
}
